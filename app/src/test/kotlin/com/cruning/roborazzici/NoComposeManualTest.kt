package com.cruning.roborazzici

import androidx.test.core.app.ActivityScenario
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.github.takahirom.roborazzi.captureRoboImage
import org.junit.Test

import org.junit.runner.RunWith
import org.robolectric.annotation.GraphicsMode

@RunWith(AndroidJUnit4::class)
@GraphicsMode(GraphicsMode.Mode.NATIVE)
class NoComposeManualTest {
    @Test
    fun captureRoboImageSample() {
        ActivityScenario.launch(MainActivity::class.java)
        val buttonFragmentOne = onView(withId(R.id.button))
        buttonFragmentOne.captureRoboImage(
            filePath = "src/test/screenshots/${FragmentOne::class.java.simpleName}_button.png",
        )
        buttonFragmentOne.perform(click())
        val buttonFragmentTwo = onView(withId(R.id.button))
        buttonFragmentTwo.captureRoboImage(
            filePath = "src/test/screenshots/${FragmentTwo::class.java.simpleName}_button.png",
        )
    }

    companion object {
        private const val weight = 360
        private const val height = 640
        private const val qualifiers = "w${weight}dp-h${height}dp"
    }
}